package process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Payload;


public class CdrWriter {
	private static Logger logger = LoggerFactory.getLogger(CdrWriter.class);
	private static String GZ_EXT = ".gz";
	
	protected volatile int chunkSize = 0;
		
	protected volatile String fileName = null;
	protected String folder = "d:/Temp/hadoop-before/";
	
	private RowCounter rowCounter;
	private int chunkResetSize = 600;
	
	
	public CdrWriter(RowCounter rowCounter) {
		this.rowCounter = rowCounter;
	}

	
	public synchronized void saveBulk(@Payload String payload,@Header String shortFileName, @Header Integer rowCount) throws Exception{
		if(this.fileName == null)
			this.fileName = generateFileName();
		
		GZIPOutputStream os = new GZIPOutputStream(new FileOutputStream(this.fileName, true));		
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(os), 10*1024);
				
		out.write(payload);
		
		this.rowCounter.incrementWriteCount(shortFileName, rowCount);
		this.chunkSize++;
		
		
		if(chunkSize % this.chunkResetSize == 0){
			this.fileName = generateFileName();
		}
		
		out.close();		
	}
	
	private String generateFileName(){
		if(!this.folder.endsWith(File.separator))
			this.folder += File.separator;
		
		return this.folder + UUID.randomUUID().toString() + CdrWriter.GZ_EXT;
	}

	public void setFolder(String folder) {
		this.folder = folder;
		this.fileName = generateFileName();
	}
	
	public void setChunkResetSize(int chunkResetSize) {
		logger.debug("Chunk Reset Size is set to " + chunkResetSize);
		this.chunkResetSize = chunkResetSize;
	}
	
}
