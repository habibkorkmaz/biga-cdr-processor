package process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CdrParserJobStep extends AbstractJobStep{

	private static Logger logger = LoggerFactory.getLogger(CdrParserJobStep.class);
	private String cdrDate;
	private CdrParserController cdrParserController;

	public CdrParserJobStep(String cdrDate, CdrParserController cdrParserController) {
		this.cdrDate = cdrDate;
		this.cdrParserController = cdrParserController;
	}


	@Override
	public void init() {
			
	}
	
	
	public void start() {	
		try {
			cdrParserController.startProcess(cdrDate);			
		} catch (Exception exc) {
			logger.error("", exc);
		}
	}
	
	public void cleanup() {
	
	}
	
	@Override
	public String toString() {
		return String.format("Cdr Processing %s", this.cdrDate);
	}

	@Override
	public boolean isRunning() {
		return cdrParserController.isRunning();
	}	
}
