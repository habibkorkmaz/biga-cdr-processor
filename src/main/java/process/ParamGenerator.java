package process;

import model.CdrFile;

import org.apache.commons.io.FilenameUtils;
import org.springframework.integration.annotation.Payload;
import org.springframework.stereotype.Component;

@Component("paramGenerator")
public class ParamGenerator {
	
	public String decideCdrType(@Payload String fileName){
		CdrFile cdrFile = CdrFile.getInstance(fileName);
		return cdrFile.getType().getType();
	}
	
	public String decideCdrDate(@Payload String fileName){
		CdrFile cdrFile = CdrFile.getInstance(fileName);		
		return cdrFile.getDate(); 
	}
	
	public String decideCdrParser(@Payload String fileName){
		CdrFile cdrFile = CdrFile.getInstance(fileName);		
		return cdrFile.getType().getParserName(); 
	}
	
	public String decideShortFileName(@Payload String fileName){
		return FilenameUtils.getName(fileName);
	}
}
