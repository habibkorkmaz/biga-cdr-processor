package process;

import java.io.File;
import java.io.FileFilter;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.endpoint.SourcePollingChannelAdapter;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.RegexPatternFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.sftp.filters.SftpRegexPatternFileListFilter;
import org.springframework.integration.sftp.inbound.SftpInboundFileSynchronizer;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import process.RowCounter.FileStatistics;

import com.jcraft.jsch.ChannelSftp.LsEntry;

public class CdrParserController implements BeanFactoryAware{
private static Logger logger = LoggerFactory.getLogger(CdrParserJobStep.class);
	
	private SourcePollingChannelAdapter fileInboundAdapter;
	private DefaultSftpSessionFactory sftpSessionFactory;
	private RowCounter rowCounter;
	private ThreadPoolTaskExecutor sftpPool;
	
	private String fileNameRegex;
	private String remoteDirectory;
	private String localDirectory;
	private boolean remoteMode = false;
	private String outputDirectory;
	
	private String currentFileNameRegex;
	
	private BeanFactory beanFactory;
	
	public CdrParserController(){
		
	}
	private void makeDirs(String cdrDate){
		new File(outputDirectory + File.separator + cdrDate).mkdirs();
		new File(localDirectory + File.separator + cdrDate).mkdirs();
	}
	
	public void startProcess(String cdrDate){
		try {
			this.currentFileNameRegex = String.format(fileNameRegex, cdrDate);
			
			this.makeDirs(cdrDate);
			
			if(remoteMode)
				this.getRemoteFiles(currentFileNameRegex, cdrDate);
			else
				this.getLocalFiles(currentFileNameRegex, cdrDate);

			Map<String,CdrWriter> writers = ((DefaultListableBeanFactory)beanFactory).getBeansOfType(CdrWriter.class);
			
			for(CdrWriter writer:writers.values()){
				writer.setFolder(outputDirectory + File.separator + cdrDate);
			}

			FileReadingMessageSource frms = new FileReadingMessageSource();
			
			frms.setDirectory(new File(localDirectory + File.separator + cdrDate));
			frms.setFilter(createFilters(currentFileNameRegex));
			
			this.fileInboundAdapter.setSource(frms);
			
			this.fileInboundAdapter.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private CompositeFileListFilter<File> createFilters(String fileNameRegex){
		CompositeFileListFilter<File> filters = new CompositeFileListFilter<File>();
		filters.addFilter(new AcceptOnceFileListFilter<File>());
		
		RegexPatternFileListFilter regexFilter = new RegexPatternFileListFilter(fileNameRegex);
		filters.addFilter(regexFilter);
		
		SimplePatternFileListFilter simpleFilter = new SimplePatternFileListFilter("*.gz");
		filters.addFilter(simpleFilter);
		
		return filters;
	}
	
	private void getRemoteFiles(String fileNameRegex, final String cdrDate) throws Exception{
		Session<LsEntry>  s  = sftpSessionFactory.getSession();
		
		SftpRegexPatternFileListFilter filter = new SftpRegexPatternFileListFilter(fileNameRegex);
		
		LsEntry[] fileList =  s.list(remoteDirectory);
		List<LsEntry> files = filter.filterFiles(fileList);
		 
		for(LsEntry file : files){
			this.rowCounter.addNewFile(file.getFilename());
		}
		
		s.close();
		
		
		final SftpInboundFileSynchronizer fs = new SftpInboundFileSynchronizer(sftpSessionFactory);
		fs.setFilter(filter);
		fs.setRemoteDirectory(remoteDirectory);
		fs.setTemporaryFileSuffix(".writing");
		
		this.sftpPool.execute(new Runnable() {
			@Override
			public void run() {
				fs.synchronizeToLocalDirectory(new File(localDirectory + File.separator + cdrDate));
			}
		});
	}
	
	private void getLocalFiles(String fileNameRegex, String cdrDate){
		File dir = new File(localDirectory + File.separator + cdrDate);

		FileFilter fileFilter = new RegexFileFilter(fileNameRegex);
		File[] files = dir.listFiles(fileFilter);

		for(File file : files){
			this.rowCounter.addNewFile(file.getName());
		}
	}
	
	public boolean isRunning() {
		return this.rowCounter.isRunning();
	}
	
	public void cleanup() {
		logger.info("Stopping process...");
		this.rowCounter.destroy();
		this.fileInboundAdapter.stop();
		//this.sftpPool.shutdown();
	}

	public static void setLogger(Logger logger) {
		CdrParserController.logger = logger;
	}

	public void setFileInboundAdapter(SourcePollingChannelAdapter fileInboundAdapter) {
		this.fileInboundAdapter = fileInboundAdapter;
	}

	public void setSftpSessionFactory(DefaultSftpSessionFactory sftpSessionFactory) {
		this.sftpSessionFactory = sftpSessionFactory;
	}

	public void setRowCounter(RowCounter rowCounter) {
		this.rowCounter = rowCounter;
	}

	public void setSftpPool(ThreadPoolTaskExecutor sftpPool) {
		this.sftpPool = sftpPool;
	}

	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}

	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}

	public void setRemoteMode(boolean remoteMode) {
		this.remoteMode = remoteMode;
	}


	public void setFileNameRegex(String fileNameRegex) {
		this.fileNameRegex = fileNameRegex;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}
		
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
}
