package process;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

//@Component("fileStatisticsHolder")
public class RowCounter {
	
	private Map<String, FileStatistics> stats = new HashMap<String, FileStatistics>();
	
	public void addNewFile(String fileName){
		this.stats.put(fileName, new FileStatistics());
	}
	
	public synchronized void incrementReadCount(String fileName, long count){
		this.stats.get(fileName).readCount += count;
	}
	
	public synchronized void incrementWriteCount(String fileName, long count){
		this.stats.get(fileName).writeCount += count;
	}

	public long getReadCount(String fileName) {
		return this.stats.get(fileName).readCount;
	}

	public long getWroteCount(String fileName) {
		return this.stats.get(fileName).writeCount;
	}
	
	public static class FileStatistics{
		private volatile long readCount = 0;
		private volatile long writeCount = 0;
		
		public long getReadCount() {
			return readCount;
		}
		public long getWriteCount() {
			return writeCount;
		}
		
	}
	
	public void destroy(){
		this.stats = new HashMap<String, FileStatistics>();
	}
	
	public Collection<FileStatistics> getStats(){
		return this.stats.values();
	}
	
	public boolean fileExists(String fileName){
		return this.stats.containsKey(FilenameUtils.getName(fileName));
	}
	
	public boolean isRunning() {
		boolean result = false;
		
		if(this.getStats().size() == 0)
			return false;
		
		for(FileStatistics fs : this.getStats()){
			if(fs.getReadCount()  == 0 || fs.getWriteCount() == 0)
				return true;
			
			if(fs.getReadCount() > fs.getWriteCount())
				return true;
		}
		
		return result;
	}
}
