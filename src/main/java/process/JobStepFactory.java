package process;

import java.util.List;

import org.springframework.beans.factory.BeanFactoryAware;

public interface JobStepFactory{
	public List<AbstractJobStep> createJobSteps();
}
