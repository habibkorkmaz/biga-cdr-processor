package process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Headers;
import org.springframework.integration.annotation.Payload;
import org.springframework.integration.message.GenericMessage;
import org.springframework.integration.support.MessageBuilder;

import parsers.LineParser;

public class CdrReader{

	protected static Logger logger = LoggerFactory.getLogger(CdrReader.class);
	protected Map<String,LineParser> lineparsers;
	
	protected MessageChannel fileWriterChannel;
	private MessageChannel retryChannel;
	
	private boolean renameAfterSuccess = false;
	private boolean deleteAfterSuccess = false;
	
	private RowCounter rowCounter;
	
	public CdrReader(Map<String,LineParser> lineParsers, RowCounter rowCounter){
		this.lineparsers = lineParsers;
		this.rowCounter = rowCounter;
	}
	
	public void processFile(@Payload String fileName,@Header("cdrParser") String cdrParser,@Header("shortFileName") String shortFileName , 
				@Header("cdrType")String cdrType ,@Headers Map<String, Object> headers) {
		logger.info("Processing file {} - ",shortFileName);
		
//		long start = System.currentTimeMillis();
		
		File file = new File(fileName);
		GZIPInputStream  gzipInput = null;
		
		try {
			try{
				gzipInput = new GZIPInputStream(new FileInputStream(file));
			}catch(Exception exc){
				logger.info("Exception on reading file {} - Retry in 5 seconds", fileName);
				Thread.sleep(5000);
				retryChannel.send(new GenericMessage<String>(fileName));
				return;
			}
			
			BufferedReader br = new BufferedReader(new InputStreamReader(gzipInput),10*1024);
			String line;
			int i = 0;

			StringBuilder sb = new StringBuilder();

			while (((line = br.readLine()) != null)) {
				i++;
				
				LineParser parser = lineparsers.get(cdrParser);
				parser.parseLine(sb,line, cdrType);
				
				if (i % 20000 == 0) {
					rowCounter.incrementReadCount(shortFileName, i);
					
					Message msg =  MessageBuilder.withPayload(sb.toString())
													.copyHeaders(headers)
													.setHeader("rowCount", i).build();
					fileWriterChannel.send(msg);
					sb = new StringBuilder();
					i = 0;
				}
			}

			this.rowCounter.incrementReadCount(shortFileName, i);
			
			
			Message msg =  MessageBuilder.withPayload(sb.toString())
											.copyHeaders(headers)
											.setHeader("rowCount", i).build();
			fileWriterChannel.send(msg);
			
			i = 0;
			br.close();
			gzipInput.close();
			
			if(renameAfterSuccess && !deleteAfterSuccess)
				file.renameTo(new File(fileName + ".SUCCESS") );
			
			if(deleteAfterSuccess)
				file.delete();
		
		} catch (Exception e) {
			logger.error("Error on file processing ", e);
		}
		
	}

	public void setRetryChannel(MessageChannel retryChannel) {
		this.retryChannel = retryChannel;
	}

	public void setFileWriterChannel(MessageChannel fileWriterChannel) {
		this.fileWriterChannel = fileWriterChannel;
	}

	public void setRenameAfterSuccess(boolean renameAfterSuccess) {
		this.renameAfterSuccess = renameAfterSuccess;
	}

	public void setDeleteAfterSuccess(boolean deleteAfterSuccess) {
		this.deleteAfterSuccess = deleteAfterSuccess;
	}
	
	
}
