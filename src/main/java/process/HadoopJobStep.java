package process;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hadoop.HadoopOps;

public class HadoopJobStep extends AbstractJobStep{
	
	private static Logger logger = LoggerFactory.getLogger(HadoopJobStep.class);
	private boolean isRunning = false;
	
	private String cdrDate;
	private String inputFolder;
	private HadoopOps hadoopOps;
	
	public HadoopJobStep(String cdrDate, String inputFolder, HadoopOps hadoopOps){
		this.cdrDate = cdrDate;
		this.inputFolder = inputFolder;
		this.hadoopOps = hadoopOps;
	}

	@Override
	public void init() {
		
	}

	public void start() {
		this.isRunning = true;
		
		try {
			hadoopOps.loadData(this.inputFolder + File.separator + cdrDate + File.separator + "*.gz", "cdr_partitioned", cdrDate);
			hadoopOps.convertRCFile(cdrDate);
			isRunning = false;
		} catch (Exception e) {
			logger.info("", e);
			this.isRunning = false;
		}
	}

	public void cleanup() {
		
	}

	public boolean isRunning() {
		return isRunning;
	}
	
	@Override
	public String toString() {
		return String.format("Hadoop Step %s", cdrDate);
	}
}
