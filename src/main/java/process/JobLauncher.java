package process;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.message.GenericMessage;

//@Component("job")
public class JobLauncher {
	
	private static Logger logger = LoggerFactory.getLogger(JobLauncher.class);
	
	private List<AbstractJobStep> jobSteps = new ArrayList<AbstractJobStep>();
	private AbstractJobStep currentStep;
	
	private List<JobStepFactory> jobStepFactories = new ArrayList<JobStepFactory>();
	
	private boolean isJobRunning = false;

	
	public JobLauncher(){}
	
	public JobLauncher(List<JobStepFactory> jobStepFactories){
		this.jobStepFactories = jobStepFactories;
	}
	
	//@Scheduled(cron="*/30 * * * * *")
	public void startJob(){
		StringBuilder statsHolder = new StringBuilder();
		
		for(JobStepFactory jsm : this.jobStepFactories){
			this.jobSteps.addAll(jsm.createJobSteps());
		}
		
		logger.info("Starting job for job steps : {}", this.jobSteps);
		
		
		if(isJobRunning){
			logger.info("Previous job not completed, retry later");
			return;
		}
		
		this.isJobRunning = true;
		
		for(AbstractJobStep js : jobSteps){
			currentStep = js;
			
			logger.info("Starting job step : {}", currentStep.toString());
			
			
			Stats stepStats =  new Stats(currentStep.toString(), new Date());
			currentStep.init();
			currentStep.start();
			
			do{
				try {
					Thread.sleep(1000 * 10);
					logger.info("Still running : {}", currentStep.toString());
				} catch (InterruptedException e) {
					logger.error("", e);
				}
			}while(currentStep.isRunning());
			
			currentStep.cleanup();
			stepStats.setEndDate(new Date());
			statsHolder.append(stepStats + "\n");
		}
		
		this.isJobRunning = false;
		logger.info("Finished job for job steps : {}", this.jobSteps);
		this.jobSteps.clear();
		
		//mailChannel.send(new GenericMessage<String>(statsHolder.toString()));
	}
	
	public void addJobStep(AbstractJobStep step){
		this.jobSteps.add(step);
	}
	
	static class Stats{
		private String stepName;
		private Date startDate;
		private Date endDate;
		
		public Stats(String stepName) {
			this.stepName = stepName;
		}

		public Stats(String stepName, Date startDate) {
			this.stepName = stepName;
			this.startDate = startDate;
		}

		public void setStartDate(Date startDate) {
			this.startDate = startDate;
		}

		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}

		@Override
		public String toString() {
			return "stepName=" + stepName + ", startDate=" + startDate
					+ ", endDate=" + endDate;
		}		
	}	
}
