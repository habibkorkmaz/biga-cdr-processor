package process;

import hadoop.HadoopOps;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import util.ParameterParser;


public class DailyJobStepFactory implements JobStepFactory {
	private static Logger logger = LoggerFactory.getLogger(DailyJobStepFactory.class);
	
	private String cdrDates;
	private CdrParserController cdrParserController;
	private String inputFolder;
	private HadoopOps hadoopOps;

	public DailyJobStepFactory(String cdrDates, CdrParserController cdrParserController, String inputFolder, HadoopOps hadooOps) {
		this.cdrDates = cdrDates;
		this.cdrParserController = cdrParserController;
		this.inputFolder = inputFolder;
		this.hadoopOps = hadooOps;
	}

	@Override
	public List<AbstractJobStep> createJobSteps() {

		List<AbstractJobStep> jobSteps = new ArrayList<AbstractJobStep>();
		List<String> pDates = ParameterParser.parseDataParam(cdrDates);
		for (String cdrDate : pDates) {
			
			CdrParserJobStep cps = new CdrParserJobStep(cdrDate, cdrParserController);
			jobSteps.add(cps);
			
			HadoopJobStep hjs = new HadoopJobStep(cdrDate, inputFolder, hadoopOps);
			jobSteps.add(hjs);
		}

		return jobSteps;
	}
}
