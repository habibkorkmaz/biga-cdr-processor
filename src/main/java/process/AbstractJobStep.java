package process;

import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;

public abstract class AbstractJobStep {

	private MessageChannel channel;

	public void sendMessage(Message<?> message){
		channel.send(message);
	}
	
	public abstract void init();

	public abstract void start();

	public abstract void cleanup();

	public abstract boolean isRunning();

	
	protected void setChannel(MessageChannel channel) {
		this.channel = channel;
	}

}
