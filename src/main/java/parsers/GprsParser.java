package parsers;

import org.springframework.stereotype.Component;

@Component("gprsParser")
public class GprsParser implements LineParser {

	public void parseLine(StringBuilder sb, String line, String cdrType) {

		sb.append(line.substring(124, 129).trim()) // cellid1
				.append(",").append(line.substring(32, 46).trim()) // aveanumber
				.append(",").append(line.substring(120, 122)) // city1
				.append(",").append("") // cellid2
				.append(",").append("") // other number
				.append(",").append("") // city2
				.append(",").append(line.substring(91, 99)) // calldate
				.append(",").append(line.substring(99, 105)) // calltime
				.append(",").append(cdrType) // cdrtypr
				.append(",").append(line.substring(381, 401).trim()) // url
				.append(",").append(line.substring(105,111).trim()) //duration
				.append("\n");

		// return cellId2 + "," + otherNumber + "," + city2 + "," + callDate +
		// "," + callTime + "," + cdrType + "," + url;
	}

}
