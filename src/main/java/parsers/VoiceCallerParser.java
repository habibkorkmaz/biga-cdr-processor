package parsers;

import org.springframework.stereotype.Component;

@Component("voiceCallerParser")
public class VoiceCallerParser implements LineParser {

	public void parseLine(StringBuilder sb, String line, String cdrType) {

		sb.append(line.substring(122, 127).trim()) // cellid1
				.append(",").append(line.substring(292, 312).trim()) // aveanumber
				.append(",").append(line.substring(256, 258)) // city1
				.append(",").append(line.substring(127, 132).trim()) // cellid2
				.append(",").append(line.substring(312, 332).trim()) // othernumber
				.append(",").append(line.substring(266, 268)) // city2
				.append(",").append(line.substring(144, 152)) // calldate
				.append(",").append(line.substring(222, 228)) // calltime
				.append(",").append(cdrType) // cdrtypr
				.append(",").append("") // url
				.append(",").append(line.substring(132,138).trim()) //duration
				.append("\n");

		
//		return cellId1 + "," + aveaNumber + "," + city1 + "," + cellId2 + ","
//				+ otherNumber + "," + city2 + "," + callDate + "," + callTime
//				+ "," + cdrType + "," + url;
	}

}
