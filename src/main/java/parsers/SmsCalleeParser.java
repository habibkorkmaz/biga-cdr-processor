package parsers;

import org.springframework.stereotype.Component;


@Component("smsCalleeParser")
public class SmsCalleeParser implements LineParser{

	public void parseLine(StringBuilder sb, String line, String cdrType) {		
		sb.append(line.substring(97, 102).trim()) // cellid1
				.append(",").append(line.substring(220, 240).trim()) // aveanumber
				.append(",").append(line.substring(197, 199)) // city1
				.append(",").append("") // cellid2
				.append(",").append(line.substring(240,260).trim()) // othernumber
				.append(",").append("") // city2
				.append(",").append(line.substring(102, 110)) // calldate
				.append(",").append(line.substring(185, 191)) // calltime
				.append(",").append(cdrType) // cdrtypr
				.append(",").append("") // url
				.append(",").append("") //duration
				.append("\n");
//		return cellId1 +"," + aveaNumber + "," + city1 + "," + cellId2 + "," + otherNumber + "," + city2 + "," + callDate + "," + callTime + "," + cdrType + "," + url;
	}

}
