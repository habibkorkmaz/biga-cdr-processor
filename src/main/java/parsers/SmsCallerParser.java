package parsers;

import org.springframework.stereotype.Component;


@Component("smsCallerParser")
public class SmsCallerParser implements LineParser{

	public void parseLine(StringBuilder sb,String line, String cdrType) {		
		sb.append(line.substring(97, 102).trim()) // cellid1
				.append(",").append(line.substring(198, 218).trim()) // aveanumber
				.append(",").append(line.substring(175, 177)) // city1
				.append(",").append("") // cellid2
				.append(",").append(line.substring(218,238).trim()) // othernumber
				.append(",").append("") // city2
				.append(",").append(line.substring(103, 111)) // calldate
				.append(",").append(line.substring(161, 167)) // calltime
				.append(",").append(cdrType) // cdrtypr
				.append(",").append("") // url
				.append(",").append("") //duration
				.append("\n");
		
		
		//return cellId1 +"," + aveaNumber + "," + city1 + "," + cellId2 + "," + otherNumber + "," + city2 + "," + callDate + "," + callTime + "," + cdrType + "," + url;
	}

}
