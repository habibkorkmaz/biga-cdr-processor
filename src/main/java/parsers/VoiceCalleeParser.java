package parsers;

import org.springframework.stereotype.Component;

@Component("voiceCalleeParser")
public class VoiceCalleeParser implements LineParser{

	public void parseLine(StringBuilder sb, String line, String cdrType) {

		sb.append(line.substring(122, 127).trim()) // cellid1
				.append(",").append(line.substring(331, 351).trim()) // aveanumber
				.append(",").append(line.substring(295, 297)) // city1
				.append(",").append(line.substring(127, 132).trim()) // cellid2
				.append(",").append(line.substring(351, 371).trim()) // othernumber
				.append(",").append(line.substring(305, 307)) // city2
				.append(",").append(line.substring(139, 147)) // calldate
				.append(",").append(line.substring(261, 267)) // calltime
				.append(",").append(cdrType) // cdrtypr
				.append(",").append("") // url
				.append(",").append(line.substring(132,138).trim()) //duration
				.append("\n");

//		return cellId1 + "," + aveaNumber + "," + city1 + "," + cellId2 + ","
//				+ otherNumber + "," + city2 + "," + callDate + "," + callTime
//				+ "," + cdrType + "," + url;
	}

}
