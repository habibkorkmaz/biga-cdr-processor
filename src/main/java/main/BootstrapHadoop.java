package main;

import hadoop.HadoopOps;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import process.JobLauncher;

public class BootstrapHadoop {

	public static void main(String[] args) {
//		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:/resources/contextMongo.xml");
//		context.getBean("jobLauncher",JobLauncher.class).startJob();
		

		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:contextMain.xml");
		HadoopOps hadoopOps = context.getBean("hadoopOps",HadoopOps.class);

//		hadoopOps.loadData("/data/cdr201110/0*.gz", "cdr_stg_test", "20121010");
		try {
			hadoopOps.convertRCFile("20140201");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Closing context");
		
		context.registerShutdownHook();
		context.close();
	}
}
