package main;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BootstrapFileParser {
	private static Logger logger = LoggerFactory.getLogger(BootstrapFileParser.class);
	
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:contextMain.xml");
		logger.info("Application started, waiting for cron trigger...");
		
	}
}
