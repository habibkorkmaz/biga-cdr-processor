package model;

public enum CdrType {
	VOICE_CALLER("mmo","voice_caller"),
	VOICE_CALLEE("mmt","voice_callee"),
	SMS_CALLER("msmo","sms_caller"),
	SMS_CALLEE("msmt","sms_callee"),
	GPRS("gprs","gprs");
	
	private String type;
	private String parserName;
	
	CdrType(String type, String parserName){
		this.type = type;
		this.parserName = parserName;
	}
	
	public String getType(){
		return type;
	}
	
	public String getParserName(){
		return this.parserName;
	}
}
