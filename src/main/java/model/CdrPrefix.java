package model;

public interface CdrPrefix {
	static String VOICE_CALLER = "s_gsm_mmo-1.1";
	static String VOICE_CALLEE = "s_gsm_mmt-1.1";
	static String SMS_CALLER = "s_gsm_msmo-1.1";
	static String SMS_CALLEE = "s_gsm_msmt-1.1";
	static String GPRS = "s_gsm-sgsn-1.1";	
}
