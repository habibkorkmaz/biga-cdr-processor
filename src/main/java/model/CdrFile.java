package model;

public class CdrFile {
	public static String VOICE = "voice";
	public static String SMS = "sms";
	public static String GPRS = "gprs";
	
	private String prefix;
	private CdrType type;
	
	private String fileName;
	
	
	private CdrFile(String fileName){
		
		if(fileName.contains(CdrPrefix.VOICE_CALLEE)){ 
			this.prefix = CdrPrefix.VOICE_CALLEE;
			this.type = CdrType.VOICE_CALLEE;
		}
		else if(fileName.contains(CdrPrefix.VOICE_CALLER)){
			this.prefix = CdrPrefix.VOICE_CALLER;
			this.type = CdrType.VOICE_CALLER;
		}
		else if(fileName.contains(CdrPrefix.SMS_CALLEE)){
			this.prefix = CdrPrefix.SMS_CALLEE;
			this.type = CdrType.SMS_CALLEE;
		}
		else if(fileName.contains(CdrPrefix.SMS_CALLER)){
			this.prefix = CdrPrefix.SMS_CALLER;
			this.type = CdrType.SMS_CALLER;
		}
		else if(fileName.contains(CdrPrefix.GPRS)){
			this.prefix = CdrFile.GPRS;
			this.type = CdrType.GPRS;
		}
		else
			throw new RuntimeException("CDR file not found, fileName" + fileName );
		
		this.fileName = fileName;
	}
	
	public static CdrFile getInstance(String fileName){
		return new CdrFile(fileName);
	}
	
	public CdrType getType(){
		return this.type;
	}
	
	public String getDate(){
		int start = this.fileName.indexOf(this.prefix) + this.prefix.length() + 1;
		return this.fileName.substring(start, start + 8);
	}
}
