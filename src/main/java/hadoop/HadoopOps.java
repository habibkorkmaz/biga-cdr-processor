package hadoop;

import org.apache.hadoop.hive.service.HiveClient;
import org.apache.hadoop.hive.service.HiveServerException;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.hadoop.hive.HiveClientFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


@Component("hadoopOps")
public class HadoopOps {
	
	static Logger logger = LoggerFactory.getLogger(HadoopOps.class);
	
	private String loadToStgQuery = "load data local inpath '%s' overwrite into table %s partition(calldate='%s') ";
	
	@Autowired
	private JdbcTemplate hiveTemplate;

	@Autowired
	private HiveClientFactory hiveClientFactory;
	
	public void loadData(String path, String table, String cdrDate){
		hiveTemplate.execute(String.format(loadToStgQuery, path, table, cdrDate));
	}
	
	public void convertRCFile(String cdrDate) throws Exception{
		
		HiveClient hiveClient = hiveClientFactory.getHiveClient();
		
		hiveClient.execute("set mapred.job.priority=VERY_HIGH");
		hiveClient.execute("SET hive.exec.dynamic.partition.mode=nonstrict");
		hiveClient.execute("SET hive.exec.max.dynamic.partitions.pernode=500");
		hiveClient.execute("SET hive.exec.compress.output=true");
		hiveClient.execute("SET mapred.max.split.size=512000000");
		hiveClient.execute("SET mapred.output.compression.type=BLOCK");
		hiveClient.execute("SET mapred.output.compression.codec=org.apache.hadoop.io.compress.SnappyCodec");

		
		logger.info("Filling cdr_partitioned_rc : {}", cdrDate);

		String sql = " INSERT OVERWRITE TABLE cdr_partitioned_new PARTITION (calldate, city1)"
				    +" SELECT aveanumber,cellid1,othernumber,cellid2,city2,calltime,cdrtype,url,duration,calldate,city1" 
				    +" FROM cdr_partitioned WHERE calldate = '%s' cluster by calldate, city1";
		
		hiveClient.execute(String.format(sql, cdrDate));
		
		
		String dropSql = String.format("ALTER TABLE cdr_partitioned drop partition (calldate='%s')", cdrDate);
		hiveClient.execute(dropSql);
	}
}
