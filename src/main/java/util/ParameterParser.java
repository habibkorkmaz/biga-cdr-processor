package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ParameterParser {
	
	public static List<String> parseDataParam(String pDates){
		String dates[] = pDates.split(",");
		List<String> cdrDateList = new ArrayList<String>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		for (String s : dates) {
			if (!s.contains("-") && !s.contains("NOW")) {
				cdrDateList.add(s);
			}
			else if(s.contains("NOW")){
				int diff = Integer.parseInt( s.replace("NOW", ""));	
				Calendar c = Calendar.getInstance();
				c.add(Calendar.DATE, diff);
				cdrDateList.add(sdf.format(c.getTime()));
			} else {
				String start = s.split("-")[0];
				String end = s.split("-")[1];
				
				Date startDate = null;
				Date endDate = null;

				try {
					startDate = sdf.parse(start);
					endDate = sdf.parse(end);
				} catch (ParseException e) {
					e.printStackTrace();
					continue;
				}

				cdrDateList.add(start);

				Calendar c = Calendar.getInstance();
				c.setTime(startDate);

				while (!c.getTime().equals(endDate)) {
					c.add(Calendar.DATE, 1);
					cdrDateList.add(sdf.format(c.getTime()));
				}
			}
		}

//		Collections.sort(cdrDateList, Collections.reverseOrder());
		return cdrDateList;
	} 
}
