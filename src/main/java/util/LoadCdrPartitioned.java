package util;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import util.ParameterParser;

@Component("loadCdrPartitioned")
public class LoadCdrPartitioned {
	private static Logger logger = LoggerFactory.getLogger(LoadCdrPartitioned.class);
	
	@Autowired
	private JdbcTemplate hiveTemplate;
	
	
	public void load(String cdrDates){
		List<String> dates = ParameterParser.parseDataParam(cdrDates);
		
		for(String date : dates){
			logger.info("Starting : {}", date);
//			hadoopOps.loadData("/gecici2/cdr-output/" + date + "/*.gz", "cdr_partitioned", date);
				
//			hiveTemplate.execute("alter table cdr_partitioned drop partition(calldate=\""+date+"\")");
			hiveTemplate.execute("load data local inpath '/data/2/cdr-output/"+date+"/*.gz' overwrite into table cdr_partitioned partition(calldate='"+date+"')");
			logger.info("Finished : {}", date);
		}
	}
	
	
	public static void main(String[] args ){
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:contextMain.xml");
		LoadCdrPartitioned load = context.getBean("loadCdrPartitioned", LoadCdrPartitioned.class);
		load.load("20131127,20131128,20131129,20131130");
	}
}
